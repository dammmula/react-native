import React from 'react';
import ContactListView from "./src/views/ContactListView";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator} from "@react-navigation/stack";
import ContactView from "./src/views/ContactView";

const Stack = createStackNavigator();

export default function App() {
  return (
      <SafeAreaProvider>
          <NavigationContainer>
              <Stack.Navigator initialRouteName='Contact List'>
                  <Stack.Screen name="Contact List" component={ContactListView} />
                  <Stack.Screen name="Contact" component={ContactView} />
              </Stack.Navigator>
          </NavigationContainer>
      </SafeAreaProvider>
  );
}
