import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Icon} from 'react-native-elements';


export default function ContactItemView(props) {

    const {navigation, name, id, imageAvailable, phoneNumbers} = props;
    const imageUri = imageAvailable ?
        props.image.uri :
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2hxVBt-_VQBS_-DLv_Nl5ZyiYWeAJF2KErw&usqp=CAU';

    const showInfo = () => {
        navigation.push('Contact', {
            name,
            imageUri,
            phoneNumbers,
            id
        })
    }

    return (
        <TouchableOpacity style={styles.contactItem}
              onPress={showInfo}>
            <View style={styles.info}>
                <Image
                    style={styles.image}
                    source={{uri: imageUri}} />
                <Text style={styles.name}>{name}</Text>
            </View>
            <Icon
                name='info'
                type='material'
                color='#999'
            />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    contactItem: {
        flex: 1,
        backgroundColor: '#ccc',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 5,
        borderRadius: 5,
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    info: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10
    },
    name: {
        flex: 1,
        flexWrap: 'wrap'
    }
});


