import React from "react";
import {TextInput, View, StyleSheet} from "react-native";
import {Icon} from "react-native-elements";


export default function SearchBarView(props) {
    const {inputValue, clearInput, searchContact} = props;

    return (
        <View style={styles.searchBar}>
            <TextInput
                style={styles.textInput}
                placeholder='Search'
                defaultValue={inputValue}
                onChangeText={(text) => searchContact(text)}/>
            <Icon
                name='clear'
                type='material'
                color='#abc'
                style={styles.icon}
                onPress={clearInput}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    searchBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,

    },
    textInput: {
        padding: 5,
        borderColor: '#abc',
        borderWidth: 1,
        borderRadius: 15,
        width: '85%'
    },
    icon: {
        marginLeft: 10
    }
})