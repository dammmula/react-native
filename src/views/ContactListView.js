import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import ContactItemView from "./ContactItemView";
import SearchBarView from "./SearchBarView";
import {Button} from 'react-native-elements';
import * as Contacts from 'expo-contacts';


export default function ContactListView({navigation}) {
    const [allContacts, setAllContacts] = useState([]);
    const [contacts, setContacts] = useState([]);
    const [inputValue, setInputValue] = useState('');

    useEffect(() => {
        (async () => await renderContacts())();
    }, []);

    const renderContacts = async () => {
        const { status } = await Contacts.requestPermissionsAsync();
        if (status === 'granted') {
            const { data } = await Contacts.getContactsAsync();

            if (data.length > 0) {
                const contacts = Array.from(data).filter((item) => {
                    return item.id && item.name && item.phoneNumbers;
                });

                setAllContacts(contacts);
                setContacts(contacts);
            }
        }
    }

    const renderItem = ({item}) => {
        return (
            <ContactItemView
            navigation={navigation}
            {...item}
            key={item.id} />
        );
    }

    const searchContact = (text) => {
        setInputValue(text);

        const newContacts = allContacts.filter(contact => {
            const name = contact.name.toLowerCase()
            return name.startsWith(text.toLowerCase());
        });
        setContacts(newContacts);
    }

    const clearInput = () => {
        setInputValue('');
        setContacts(allContacts);
    }

    const addContact = async () => {
        await Contacts.presentFormAsync(null, {}, {isNew: true});
        await renderContacts();
    }

    return (
        <View style={styles.contactList}>

            <SearchBarView
                searchContact={searchContact}
                inputValue={inputValue}
                clearInput={clearInput}/>

            <FlatList data={contacts} renderItem={renderItem} />

            <View style={styles.footer}>
                <Button
                    buttonStyle={styles.button}
                    onPress={addContact}
                    title='Add contact'>
                </Button>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    contactList: {
        flex: 1,
        padding: 20,

    },
    button: {
        margin: 20,
        width: '80%',
        backgroundColor: '#abc',
        marginLeft: 'auto',
        marginRight: 'auto'
    }
});
