import React from "react";
import {Image, Text, View, StyleSheet} from "react-native";
import {Button, Icon} from 'react-native-elements';
import call from "react-native-phone-call";
import * as Contacts from "expo-contacts";


export default function ContactView(props) {

    const {imageUri, name, phoneNumbers, id} = props.route.params;
    const number = phoneNumbers[0].number;

    const onCallPress = () => {
        const args = {
            number: number,
            prompt: false
        }

        call(args).catch(console.error)
    }

    const onDeletePress = async () => {
        await Contacts.presentFormAsync(id, null, {isNew: false});
    }

    return (
        <View style={styles.contact}>
            <Image
                source={{
                    uri: imageUri
                }}
                style={{ width: 200, height: 200 }}
            />
            <View style={styles.contactInfo}>
                <Text>
                    Name:{'\n'}
                    {name}
                </Text>
                <Text>
                    Phone number:{'\n'}
                    {number}
                </Text>
            </View>
            <View style={styles.buttons}>
                <Button
                    icon={
                        <Icon
                            iconStyle={
                                styles.icon
                            }
                            name="call"
                        />
                    }
                    buttonStyle={styles.button}
                    title='Call'
                    onPress={onCallPress}/>
                <Button
                    iconRight={true}
                    icon={
                        <Icon
                            name="clear"
                            iconStyle={
                                styles.icon
                            }
                        />
                    }
                    buttonStyle={styles.button}
                    title='Delete'
                    onPress={onDeletePress}/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    contact: {
        height: '100%',
        width: '100%',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    contactInfo: {

    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    button: {
        margin: 20,
        padding: 5,
        backgroundColor: '#abc',
        width: 90,
        height: 40,
        justifyContent: 'space-around'
    },
    icon: {
        color: "white"
    }
})